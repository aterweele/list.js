/* Copyright (c) 2015 Alexander ter Weele
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

var List = (function() {
  return {
    /* Return the length (number of elements) of the given list.
     */
    length      : function(l) { return l.length; },

    /* returns true iff the list contains no elements.
     */
    is_nil      : function(l) { return l.length == 0; },

    /* Return the first element of the given list.
     */
    hd          : function(l) { return l[0]; },

    /* Return the given list without its first element.
     */
    tl          : function(l) { return l.slice(1); },

    /* Return the n-th element of the given list. The first element (head of
     * the list) is at position 0.
     */
    nth         : function(l, i) { return l[i] },

    /* List reversal.
     */
    rev         : function(l) {
      if (this.is_nil(l)) {
        return [];
      } else {
          return this.rev(this.tl(l)).concat([this.hd(l)]);
      }
    },

    /* Catenate two lists. Same function as the built-in .concat()
     */
    append      : function(l1, l2) { return l1.concat(l2); },

    /* List.rev_append(l1, l2) reverses l1 and concatenates it to l2.
     */
    rev_append  : function(l1, l2) {
      if (this.is_nil(l1)) {
        return l2;
      } else {
        return this.rev_append(this.tl(l1), [this.hd(l1)].concat(l2));
      }
    },

    /* Concatenate a list of lists. The elements of the argument are all
     * concatenated together (in the same order) to give the result.
     */
    concat      : function(ll) {
      if (this.length(ll) == 0) {
        return [];
      } else {
        return this.hd(ll).concat(this.concat(this.tl(ll)));
      }
    },

    /* Same as concat.
     */
    flatten     : function(ll) { return this.concat(ll); },

    /* List.iter(f, [a1, ..., an]) applies function f in turn to a1; ...; an.
     * It is equivalent to f(a1); ...; f(an);
     *
     * XXX: If the list contains primitives, iter is guaranteed not to modify
     * the list elements.  However, If the list contains objects or lists, iter
     * may modify the list elements.  This is a side effect of Javascript's
     * call-by-sharing strategy (see https://stackoverflow.com/a/5314911 ).
     * When f is applied to an element e, f may modify e[x] or e.x, but changes
     * to e itself will not "stick" after f returns.
     *
     * Despite this, the imagined use-case for iter is with a function f that
     * uses a list element, but will not actually modify it, instead producing
     * a side effect.
     */
    iter        : function(f, l) {
      if (this.is_nil(l)) {
        return;
      } else {
        f(this.hd(l));
        this.iter(f, this.tl(l));
      }
    },

    /* Same as List.iter, but the function is applied to the index of the
     * element as first argument (counting from 0), and the element itself as
     * second argument.
     *
     * XXX: the same caveats for iter apply to iteri.
     */
    iteri       : function(f, l) {
      var _ = function(i) {
        if (i == List.length(l)) {
          return;
        } else {
          var e = l[i];
          f(i, e);
          l[i] = e;
          _(i+1);
        }
      };
      _(0);
    },

    /* List.map(f, [a1, ..., an]) applies function f to a1, ..., an, and builds
     * the list [f(a1), ..., f(an)] with the results returned by f.
     */
    map         : function(f, l) { return l.map(f); },

    /* Same as List.map, but the function is applied to the index of the
     * element as first argument (counting from 0), and the element itself as
     * second argument. 
     */
    mapi        : function(f, l) {
      var _ = function(n, f, l) {
        if (List.is_nil(l)) {
          return [];
        } else {
          return [f(n, List.hd(l))].concat(_(n+1, f, List.tl(l)));
        }
      };
      return _(0, f, l);
    },

    /* List.rev_map(f, l) gives the same result as List.rev(List.map(f, l))
     */
    rev_map     : function(f, l) {
      if (this.is_nil(l)) {
        return [];
      } else {
        return this.rev_map(f, this.tl(l)).concat([f(this.hd(l))]);
      }
    },

    /* List.fold_left(f, a, [b1, ..., bn]) is f(...f(f(a, b1), b2)..., bn)
     */
    fold_left   : function(f, s, l) {
      if (this.is_nil(l)) {
        return s;
      } else {
        return this.fold_left(f, f(s, this.hd(l)), this.tl(l));
      }
    },

    /* List.fold_right(f, [a1, ..., an], b) is f(a1, f(a2, ...f(an, b)...))
     */
    fold_right  : function(f, l, s) {
      if (this.is_nil(l)) {
        return s;
      } else {
        return f(this.hd(l), this.fold_right(f, this.tl(l), s));
      }
    },

    /* List.iter2(f, [a1, ..., an], [b1, ..., bn]) calls in turn
     * f(a1, b1); ...; f(an, bn).
     * iter2 will exit immediately if l1 and l2 have mismatched
     * lengths.
     *
     * XXX: the same caveats as iter apply, namely, f(ai, bi) could result in
     * the modification of an attribute of either argument if the argument is
     * an object or list.
     */
    iter2       : function(f, l1, l2) {
      if (this.length(l1) !== this.length(l2)) {
        return;
      }
      var _ = function(l1, l2) {
        if (this.is_nil(l1)) {
          return;
        } else {
          f(this.hd(l1), this.hd(l2));
          _(this.tl(l1), this.tl(l2));
        }
      };
      _(l1, l2);
    },

    /* map2(f, [a1, ..., an], [b1, ..., bn]) is
     * [f(a1, b1), ..., f(an, bn)]
     * returns null if l1 and l2 have different lengths.
     * Not tail-recursive.
     */
    map2        : function(f, l1, l2) {
      if (this.is_nil(l1) !== this.is_nil(l2)) {
        return null;
      } else if (this.is_nil(l1) && this.is_nil(l2)) {
        return [];
      } else {
        var _ = this.map2(f, this.tl(l1), this.tl(l2));
        if ( _ === null) {
          // lists of differing lengths.  Give up.
          return null;
        }
        return [f(this.hd(l1), this.hd(l2))].concat(_);
      }
    },

    /* for_all(p, [a1, ..., an]) checks if all elements of the list satisfy the
     * predicate p. That is, it returns p(a1) && p(a2) && ... && p(an).
     */
    for_all     : function(p, l) {
      if (this.is_nil(l)) {
        return true;
      } else {
        return p(this.hd(l)) && this.for_all(p, this.tl(l));
      }
    },

    /* exists(p, [a1, ..., an]) checks if at least one element of the list
     * satisfies the predicate p. That is, it returns
     * p(a1) || p(a2) || ... || p(an).
     */
    exists      : function(p, l) {
      if (this.is_nil(l)) {
        return false;
      } else {
        return p(this.hd(l)) || this.exists(p, this.tl(l));
      }
    },

    /* Same as List.for_all, but for a two-argument predicate.
     * XXX: returns null (which is falsey) if l1 and l2 have different lengths
     */
    for_all2    : function(p, l1, l2) {
      if (this.is_nil(l1) !== this.is_nil(l2)) {
        // best solution for differing lengths.
        return null;
      } else if (this.is_nil(l1) && this.is_nil(l2)) {
        return true;
      } else {
        var _ = this.for_all2(p, this.tl(l1), this.tl(l2));
        if (_ === null) {
          // lists of differing lengths.  Give up.
          return null;
        }
        return p(this.hd(l1), this.hd(l2)) && _;
      }
    },

    /* Same as List.exists, but for a two-argument predicate.
     * XXX: returns null (which is falsey) if l1 and l2 have different lengths
     */
    exists2     : function(p, l1, l2) {
      if (this.is_nil(l1) !== this.is_nil(l2)) {
        // best solution for differing lengths.
        return null;
      } else if (this.is_nil(l1) && this.is_nil(l2)) {
        return false;
      } else {
        var _ = this.exists2(p, this.tl(l1), this.tl(l2));
        if (_ === null) {
          // lists of differing lengths.  Give up.
          return null;
        }
        return p(this.hd(l1), this.hd(l2)) || _;
      }
    },

    /* mem(a, l) is true if and only if a is equal (===) to an element of l.
     */
    mem         : function(e, l) {
      if (this.is_nil(l)) {
        return false;
      } else if (this.hd(l) === e) {
        return true;
      } else {
        return this.mem(e, this.tl(l));
      }
    },

    /* memq omitted.
     */

    /* find(p, l) returns the first element of the list l that satisfies the
     * predicate p.
     * Returns null if no such element exists.
     * XXX: there's no way to differentiate between a null in the list that
     * satisfies p and a null that indicates failure to find a satisfying
     * element.
     */
    find        : function(p, l) {
      if (this.is_nil(l)) {
        return null;
      } else if (p(this.hd(l))) {
        return this.hd(l);
      } else {
        return this.find(p, this.tl(l));
      }
    },

    /* filter(p, l) returns all the elements of the list l that satisfy the
     * predicate p. The order of the elements in the input list is preserved.
     */
    filter      : function(p, l) {
      if (this.is_nil(l)) {
        return [];
      } else if (p(this.hd(l))) {
        return [this.hd(l)].concat(this.filter(p, this.tl(l)));
      } else {
        return this.filter(p, this.tl(l));
      }
    },

    /* find_all is another name for List.filter.
     */
    find_all    : function(p, l) { return this.filter(p, l); },

    /* partition(p, l) returns an object {true: l1, false: l2}, where l1 is the
     * list of all the elements of l that satisfy the predicate p, and l2 is
     * the list of all the elements of l that do not satisfy p. The order of
     * the elements in the input list is preserved.
     */
    partition   : function(p, l) {
      var _ = function(l, o) {
        if (List.is_nil(l)) {
          return o;
        } else {
          if (p(List.hd(l))) {
            o[true].push(List.hd(l));
          } else {
            o[false].push(List.hd(l));
          }
          return _(List.tl(l), o);
        }
      };
      return _(l, {true : [], false : []});
    },

    /* Association list functions and List.split() omitted because Javascript
     * way would be to use an object instead of a list of duples, and because
     * Javascript does not have tuples.
     */

    /* Transform a pair of lists into a list of (objects that approximate)
     * pairs:
     * combine([a1, ..., an], [b1, ..., bn]);
     * is
     * [{true: a1, false: b1}, ..., {true: an, false: bn}]
     * Returns null for differing lengths
     */
    combine       : function(l1, l2) {
      if (this.is_nil(l1) !== this.is_nil(l2)) {
        // best solution for differing lengths.
        return null;
      } else if (this.is_nil(l1) && this.is_nil(l2)) {
        return [];
      } else {
        var _ = this.combine(this.tl(l1), this.tl(l2));
        if (_ === null) {
          return null;
        } else {
          return [{true: this.hd(l1), false: this.hd(l2)}].concat(_);
        }
      }
    },

    /* Sort a list in increasing order according to a comparison function. The
     * comparison function must return 0 if its arguments compare as equal, a
     * positive number if the first is greater, and a negative number if the
     * first is smaller. The resulting list is sorted in increasing order.
     *
     * The current implementation uses Merge Sort.
     */
    sort          : function(c, l) {
      if (this.is_nil(l) || this.length(l) === 1) {
        return l;
      } else {
        // should work, even though length / 2 is liable to make a non-integer.
        // ew.
        var left  = this.sort(c, l.slice(0,this.length(l) / 2));
        var right = this.sort(c, l.slice(this.length(l) / 2));
        return this.merge(c, left, right);
      }
    },

    /* Same as List.sort, but the sorting algorithm is guaranteed to be stable
     * (i.e. elements that compare equal are kept in their original order).
     * The current implementation uses Merge Sort.
     */
    stable_sort   : function(c, l) { this.sort(c, l); },

    /* fast sort on typical input, currently an synonym for sort.
     */
    fast_sort     : function(c, l) { this.sort(c, l); },

    /* Same as List.sort, but also remove duplicates.
     */
    sort_uniq     : function(c, l) {
      var f = function(l1, l2) {
        if (List.is_nil(l1) || List.is_nil(l2)) {
          return l1.concat(l2);
        } else {
          var _ = c(List.hd(l1), List.hd(l2));
          if (_ > 0) {
            return [List.hd(l2)].concat(f(l1, List.tl(l2)));
          } else if (_ == 0) {
            return [List.hd(l1)].concat(f(List.tl(l1), List.tl(l2)));
          } else {
            return [List.hd(l1)].concat(f(List.tl(l1), l2));
          }
        }
      };
      if (this.is_nil(l) || this.length(l) === 1) {
        return l;
      } else {
        var left  = this.sort_uniq(c, l.slice(0,this.length(l) / 2));
        var right = this.sort_uniq(c, l.slice(this.length(l) / 2));
        return f(c, left, right, null);
      }
    },

    /* Merge two lists: Assuming that l1 and l2 are sorted according to the
     * comparison function cmp, merge(cmp, l1, l2) will return a sorted list
     * containting all the elements of l1 and l2. If several elements compare
     * equal, the elements of l1 will be before the elements of l2.
     */
    merge         : function(c, l1, l2) {
      if (this.is_nil(l1) || this.is_nil(l2)) {
        return l1.concat(l2);
      } else {
        if (c(this.hd(l1), this.hd(l2)) >= 0) {
          return [this.hd(l2)].concat(
              this.merge(c, l1, this.tl(l2))
          );
        } else {
          return [this.hd(l1)].concat(
              this.merge(c, this.tl(l1), l2)
          );
        }
      }
    },
  }
}());
