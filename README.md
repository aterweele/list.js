List.js, List processing for Javascript
===

An MIT-licensed implementation of (most of) [OCaml's List
module](http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html) in
Javascript.  It was tested in Iceweasel 31.3.0.

Why?
---

My first functional language was OCaml, where any task involving a list can
probably be implemented with a higher-order function from its list module and
an anonymous function.  When I tried Javascript, I found I missed the
convenience of these, so I decided to bring them over.

Why not [underscore.js](http://underscorejs.org/)?
---

Underscore has a number of advantages: it is better-known, older, more
rigorously tested, and has higher-order functions that operate on collections.
That said, List.js is good for programmers with a fondness for OCaml who only
need higher-order functions for arrays.

Why are some of the List module functions not implemented?
---

Some of the List module functions wouldn't translate well to a Javascript.  For
those, I have provided explanations in comments in the source.  For example,
Javascript doesn't have the concept of structural versus physical equality, so
I have included `mem` and omitted `memq`.

What's still to do?
---

I'm considering an entirely tail-recursive version, even if it's just for fun.

Documentation doesn't specify behavior for `hd`, `tl`, and `nth` in cases where
the list is too short to handled the operation as expected.

Why don't you make any guarantees about tail recursion, stack size, etc.?
---

I wrote this in vanilla Javascript, hoping that it would run in any interpreter
that correctly implements Javascript.  That said, I don't know if a correct
Javascript implementation stipulates that functions written to be
tail-recursive actually have a constant stack size, so in copying over OCaml's
documentation, I omitted any mention of tail recursion.
